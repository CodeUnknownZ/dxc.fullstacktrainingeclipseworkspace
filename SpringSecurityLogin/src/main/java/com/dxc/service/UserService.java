package com.dxc.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.dxc.payload.UserDTO;

public interface UserService extends UserDetailsService{
	UserDTO saveUser(UserDTO userDTO);
}
