package com.dxc.dao;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.dxc.model.*;

@Component
public class EmployeeDao {

	@Autowired
	HibernateTemplate hibernateTemplate;

	@Transactional
	public Serializable addEmployee(Employee employee) {
		return hibernateTemplate.save(employee);
	}

	public List<Employee> getAllEmployee() {
		return hibernateTemplate.loadAll(Employee.class);
	}

	public Employee getEmployeeByID(long id) {
		return hibernateTemplate.load(Employee.class, id);
	}

	@Transactional
	public void updateEmployee(Employee employee) {
		hibernateTemplate.update(employee);
	}

	@Transactional
	public void deleteEmployee(long id) {
		hibernateTemplate.delete(hibernateTemplate.load(Employee.class, id));
	}
}
