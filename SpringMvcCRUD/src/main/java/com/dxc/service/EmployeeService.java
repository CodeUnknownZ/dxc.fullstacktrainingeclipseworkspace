package com.dxc.service;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.dxc.dao.EmployeeDao;
import com.dxc.model.Employee;

@Component
@Service
public class EmployeeService {
	
	@Autowired
	EmployeeDao employeeDao;
	
	@Transactional
	public Serializable addEmployee(Employee employee) {
		return employeeDao.addEmployee(employee);
	}

	public List<Employee> getAllEmployee() {
		return employeeDao.getAllEmployee();
	}

	public Employee getEmployeeByID(long id) {
		return employeeDao.getEmployeeByID(id);
	}

	@Transactional
	public void updateEmployee(Employee employee) {
		employeeDao.updateEmployee(employee);
	}

	@Transactional
	public void deleteEmployee(long id) {
		employeeDao.deleteEmployee(id);
	}
}
