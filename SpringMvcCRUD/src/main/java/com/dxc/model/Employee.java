package com.dxc.model;

import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String department;
	private String address;
	private Double salary;
	private String designation;
	
//	// One to One mapping
//	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "profile_id", referencedColumnName = "id")
//	private Profile profile;
//	
//	// Many to One mapping
//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "department_id", referencedColumnName = "id")
//	private Department department;
//	
//	// One to Many mapping
//	@OneToMany(cascade = CascadeType.ALL , orphanRemoval = true)
//	@JoinColumn(name = "employee_id", referencedColumnName = "id")
//	private List<Task> tasks;
//	
//	// Many to Many
//	@ManyToMany(cascade = CascadeType.ALL)
//	@JoinTable(name = "projects_employees")
//	private List<Project> projects;
	
}
