package org.dxc.factorydesign;

import org.dxc.service.StudentServiceImplementation;

public class ServiceFactory {
	
	private static final StudentServiceImplementation studentServiceImplementation;
	
	static {
		studentServiceImplementation = new StudentServiceImplementation();
	}

	public static StudentServiceImplementation getServiceObject() {
		return studentServiceImplementation;
	}

}
