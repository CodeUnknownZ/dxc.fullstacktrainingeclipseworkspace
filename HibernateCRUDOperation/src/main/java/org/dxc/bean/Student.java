package org.dxc.bean;

import javax.persistence.*;

@Entity
@Table(name = "STUDENTS")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static void printStudentDeatils(Student student) {
		if (student == null) {
			System.out.println("Attempting to print null record...");
		} else {
			System.out.print("| ID :  " + student.getId());
			System.out.print("\t| Name :  " + student.getName());
			System.out.println("\t| Address :  " + student.getAddress());
		}
	}

}
