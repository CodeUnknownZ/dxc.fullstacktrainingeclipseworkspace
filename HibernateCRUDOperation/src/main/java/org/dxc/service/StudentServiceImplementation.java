package org.dxc.service;

import java.util.Iterator;
import java.util.List;

import org.dxc.bean.Student;
import org.dxc.factorydesign.HibernateFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class StudentServiceImplementation implements StudentService {

	SessionFactory factory;
	Session session;
	Transaction tx = null;

	public StudentServiceImplementation() {
		this.factory = HibernateFactory.getFactoryObject();
	}

	@Override
	public Integer insert(Student s) {
		try {
			this.startTransaction();

			Integer id = (Integer) this.session.save(s);
			System.out.println("Created Student Successfully");

			this.tx.commit();
			return id;
		} catch (Exception e) {
			if (this.tx != null)
				this.tx.rollback();
			e.printStackTrace();
		} finally {
			this.endTransaction();
		}
		return null;
	}

	@Override
	public List<Student> getAllStudent() {
		try {
			this.startTransaction();

			@SuppressWarnings("unchecked")
			List<Student> students = this.session.createQuery("FROM Student").list();
			for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
				Student student = (Student) iterator.next();
				Student.printStudentDeatils(student);
			}

			if (students.isEmpty() || students == null) {
				System.err.println("No records found!");
			}

			this.tx.commit();
			return students;
		} catch (Exception e) {
			if (this.tx != null)
				this.tx.rollback();
			e.printStackTrace();
		} finally {
			this.endTransaction();
		}
		return null;
	}

	@Override
	public Student getStudentById(int id) {
		try {
			this.startTransaction();

			Student student = (Student) this.session.get(Student.class, id);
			if (student == null)
				throw new NullPointerException();

			this.tx.commit();
			return student;
		} catch (Exception e) {
			if (this.tx != null)
				this.tx.rollback();
			if (e instanceof NullPointerException) {
				System.err.println("Unable to find record of student with ID of " + id);
			} else {
				e.printStackTrace();
			}
		} finally {
			this.endTransaction();
		}
		return null;
	}

	@Override
	public Boolean updateRecord(Student s) {
		try {
			this.startTransaction();

			this.session.update(s);

			this.tx.commit();
			return (Boolean) true;
		} catch (Exception e) {
			if (this.tx != null)
				this.tx.rollback();
			e.printStackTrace();
		} finally {
			this.endTransaction();
		}
		return (Boolean) false;
	}

	@Override
	public Boolean deleteRecord(int id) {
		try {
			this.startTransaction();

			Student student = (Student) session.get(Student.class, id);
			System.err.println("Details of student below has been deleted");
			Student.printStudentDeatils(student);

			this.session.delete(student);

			this.tx.commit();
			return (Boolean) true;
		} catch (Exception e) {
			if (this.tx != null)
				this.tx.rollback();
			e.printStackTrace();
		} finally {
			this.endTransaction();
		}
		return (Boolean) false;
	}

	private void startTransaction() {
		try {
			this.session = this.factory.openSession();
			this.tx = this.session.beginTransaction();
		} catch (HibernateException e) {
			System.err.println("Error in opening the session............");
			e.printStackTrace();
		}

	}

	private void endTransaction() {
		try {
			this.session.close();
			this.tx = null;
		} catch (HibernateException e) {
			System.err.println("Error in closing the session...problems cleaning up..");
			e.printStackTrace();
		}
	}

}
