package org.dxc.service;

import java.util.List;

import org.dxc.bean.Student;

public interface StudentService {
	public static final String missingMsg = "";
	Integer insert(Student s);
	List<Student> getAllStudent();
	Student getStudentById(int id);
	Boolean updateRecord(Student s);
	Boolean deleteRecord(int id);
}
