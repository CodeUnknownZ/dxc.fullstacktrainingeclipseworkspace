package org.dxc.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.dxc.bean.Student;
import org.dxc.factorydesign.ServiceFactory;
import org.dxc.service.StudentService;

public class Main {
	static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	static String[] menuItems = { "Add Student Record", "Get Student Details", "List Of All Student Records",
			"Delete A Student Record", "Update A Student's Record", "Exit App" };

	public static void main(String[] args) {
		StudentService service = ServiceFactory.getServiceObject();
		boolean loop = true;

		while (loop) {

			Student student = new Student();
			int studentID;
			String studentName;
			String studentAddr;

			printMenu();
			System.out.print("Select operation : ");
			String switchInput = getUserInput();
			Main.printDivider();

			switch (switchInput) {
			case "1":

				// Get Student details from user input
				do {
					System.out.print("\tPlease Enter Student's Name: ");
					studentName = getUserInput();
					student.setName(studentName);
					if(studentName == null || studentName == "") System.err.println("Name cannot be empty!");
				} while (studentName == null || studentName == "");
				
				do {
					System.out.print("\tPlease Enter Student's Address: ");
					studentAddr = getUserInput();
					student.setAddress(studentAddr);
					if(studentAddr == null || studentAddr == "") System.err.println("Address cannot be empty!");
				} while (studentAddr == null || studentAddr == "");

				// Insert and print student details
				Student.printStudentDeatils(service.getStudentById(service.insert(student)));
				System.out.println("Details above has been added to the record");
				break;
			case "2":
				// get id from user
				studentID = getIDInput("\tPlease Enter Student ID: ");

				// show user details of student based on id inputed
				Main.printDivider();
				Student.printStudentDeatils(service.getStudentById(studentID));
				break;
			case "3":
				// SELECT * FROM myblog.students
				service.getAllStudent();
				break;
			case "4":
				// Ask user for student id for deletion
				int idToDelete = getIDInput("\tPlease Enter Student ID For DELETION: ");

				// confirmation in case of wrong id input
				System.out.print("\tPlease confirm by entering \"CONFIRM\": ");

				// proceed/abort deletion of record
				if (getUserInput().equals("CONFIRM")) {
					service.deleteRecord(idToDelete);
				} else {
					System.out.println("Input MISMATCH, aborting delete.");
				}
				break;
			case "5":
				// get student object from student id from user input
				studentID = getIDInput("\tPlease Enter Student ID to update: ");
				student = service.getStudentById(studentID);

				// if unable to retrieve student data return user to main menu
				if (student == null) {
					break;
				}

				// show user the current student details
				Main.printDivider();
				System.out.println("Current Student's Details: ");
				Student.printStudentDeatils(student);
				Main.printDivider();

				// ask user for updated info (if any) user can enter blank to skip
				System.out.print("\tPlease Enter NEW Student Name to update: ");
				studentName = getUserInput();
				if (!(studentName == null || studentName == "")) {
					student.setName(studentName);
				}

				System.out.print("\tPlease Enter NEW Student Address to update: ");
				studentAddr = getUserInput();
				if (!(studentAddr == null || studentAddr == "")) {
					student.setAddress(studentAddr);
				}

				// update the details of the student
				service.updateRecord(student);

				// show user the updated details
				Main.printDivider();
				System.out.println("New Student's Details: ");
				Student.printStudentDeatils(service.getStudentById(studentID));
				break;
			case "E":
			case "e":
				loop = false;
				break;
			default:
				System.err.println("Invalid Choice, Please select from the list below");
				break;
			}
		}
	}

	static void printMenu() {
		printDivider();
		for (int i = 0; i < menuItems.length - 1; i++) {
			System.out.println("Press " + (i + 1) + " --> To " + menuItems[i]);
		}
		System.out.println("Press E --> To " + menuItems[menuItems.length - 1]);
		printDivider();
	}

	static String getUserInput() {
		try {
			return reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void printDivider() {
		System.out.println("===========================================================");
	}

	private static int getIDInput(String message) {
		boolean err = false;
		int id = 0;
		do {
			System.out.print(message);
			try {
				id = Integer.parseInt(getUserInput());
			} catch (Exception e) {
				System.err.println("Input is not a number, please try again");
			}
		} while (err | id == 0);
		return id;
	}

}
