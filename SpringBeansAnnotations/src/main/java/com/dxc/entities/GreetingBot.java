package com.dxc.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dxc.service.GreetingService;

@Component
public class GreetingBot {
	
	@Autowired
	private GreetingService service;
	
	public void print() {
		System.out.println(this.service.greet());
	}
}
