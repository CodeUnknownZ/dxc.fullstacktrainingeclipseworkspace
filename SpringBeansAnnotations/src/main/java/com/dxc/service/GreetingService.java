package com.dxc.service;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class GreetingService {
	@Bean
	GreetingService mockGreetingService() {
		return new GreetingService();
	}

	public String greet() {
		return "GoodDay Mate!";
	}
}
