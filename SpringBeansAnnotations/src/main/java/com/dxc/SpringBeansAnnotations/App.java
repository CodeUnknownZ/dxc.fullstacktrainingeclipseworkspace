package com.dxc.SpringBeansAnnotations;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.dxc.entities.*;
import com.dxc.service.*;


public class App 
{
    public static void main( String[] args )
    {
    	ApplicationContext appContext = new AnnotationConfigApplicationContext(BeanConfig.class);
    	// ERROR Exception in thread "main" org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 'com.dxc.entities.GreetingBot' available
    	// at com.dxc.SpringBeansAnnotations.App.main(App.java:17)
    	GreetingBot greeter = (GreetingBot) appContext.getBean(GreetingBot.class);
		greeter.print();
    }
}

@Configuration
@ComponentScan
class BeanConfig {
	@Bean
	GreetingService mockGreetingService() {
		return new GreetingService() {
			public String greet() {
				return "GoodDay Mate!";
			}
		};
	}
}