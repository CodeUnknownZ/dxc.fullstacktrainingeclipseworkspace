import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
    {
        path: "login",
        component: MainComponent,
        title: "Login Page",
    }, {
        path: '',
        component: MainComponent,
        title: "Login Page",
    }, {
        path: '**',
        component: ErrorComponent,
        title: "Error",
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
