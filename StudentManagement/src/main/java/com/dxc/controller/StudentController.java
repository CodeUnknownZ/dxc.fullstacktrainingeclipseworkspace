package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.dxc.entity.Student;
import com.dxc.service.StudentService;


@Controller
@RequestMapping
public class StudentController {

	@Autowired
	private StudentService studentService;
	
//	public StudentController(StudentService studentService) {
//		super();
//		this.studentService = studentService;
//	}
	
	@RequestMapping("/")
	public String viewHome() {
		return "redirect:/students";
	}
	
	@GetMapping("/students")
	public String listStudents(Model model) {
		model.addAttribute("students", studentService.getAll());
		return "students";
	}
	
	@GetMapping("/students/new")
	public String createStudentForm(Model model) {
		Student student = new Student();
		model.addAttribute("student", student);
		return "create_student";
	}
	
	@PostMapping("/students")
	public String saveStudent(@ModelAttribute("student") Student student) {
		studentService.save(student);
		return "redirect:/students";
	}
	
	@GetMapping("/students/edit/{id}")
	public String editStudentForm(@PathVariable Long id, Model model) {
		model.addAttribute("student", studentService.getById(id));
		return "edit_student";
	}
	
	@PostMapping("/students/{id}")
	public String updateStudent(@PathVariable Long id,
			@ModelAttribute("student") Student student,
			Model model) {
		
		// get student from database by id
		Student existingStudent = studentService.getById(id);
		existingStudent.setId(id);
		existingStudent.setFirstName(
				student.getFirstName().isBlank() ? existingStudent.getFirstName() : student.getFirstName());
		existingStudent.setLastName(
				student.getLastName().isBlank() ? existingStudent.getLastName() : student.getLastName());
		existingStudent.setEmail(
				student.getEmail().isBlank() ? existingStudent.getEmail() : student.getEmail());
		
		// save updated student object
		studentService.update(existingStudent);
		return "redirect:/students";		
	}
	
	@GetMapping("/students/{id}")
	public String deleteStudent(@PathVariable Long id) {
		studentService.deleteById(id);
		return "redirect:/students";
	}
}
