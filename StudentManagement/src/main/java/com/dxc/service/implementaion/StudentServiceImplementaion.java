package com.dxc.service.implementaion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dxc.entity.Student;
import com.dxc.repository.StudentRepository;
import com.dxc.service.StudentService;

@Service
public class StudentServiceImplementaion implements StudentService{
	
	@Autowired
	private StudentRepository studentRepository;
	
	public StudentServiceImplementaion(StudentRepository studentRepository) {
		super();
		this.studentRepository = studentRepository;
	}

	@Override
	public List<Student> getAll() {
		return this.studentRepository.findAll();
	}

	@Override
	public Student save(Student student) {
		return this.studentRepository.save(student);
	}

	@Override
	public Student getById(Long id) {
		return this.studentRepository.findById(id).get();
	}

	@Override
	public Student update(Student student) {
		return this.studentRepository.save(student);
	}

	@Override
	public void deleteById(Long id) {
		this.studentRepository.deleteById(id);
	}

}
