package com.dxc.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dxc.entity.Student;

public interface StudentService {
	List<Student> getAll();

	Student save(Student student);

	Student getById(Long id);

	Student update(Student student);

	void deleteById(Long id);
}
