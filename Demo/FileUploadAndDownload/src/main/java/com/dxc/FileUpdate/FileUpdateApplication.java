package com.dxc.FileUpdate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileUpdateApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileUpdateApplication.class, args);
	}

}
