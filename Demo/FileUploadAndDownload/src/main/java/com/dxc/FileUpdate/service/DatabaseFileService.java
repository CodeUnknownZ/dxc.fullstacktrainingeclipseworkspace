package com.dxc.FileUpdate.service;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dxc.FileUpdate.exception.FileStorageException;
import com.dxc.FileUpdate.model.DatabaseFile;
import com.dxc.FileUpdate.repository.DatabaseFileRepository;

@Service
public class DatabaseFileService {
	@Autowired
	private DatabaseFileRepository dbFileRespository;
	
	public DatabaseFile storeFile(MultipartFile file) {
		String fileName=StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if(fileName.contains("..")) {
				throw new FileStorageException(
						"Sorry! FileName contains invalid path Sequence"+fileName
						);
			}
			DatabaseFile dbFile=new DatabaseFile(fileName,file.getContentType(),file.getBytes());
		    return dbFileRespository.save(dbFile);
		}catch(IOException e) {
			throw new FileStorageException("Could not Store File"+fileName+"please try again",e);
		}
		
	}
	
	public DatabaseFile getFile(String fileId) {
		return dbFileRespository.findById(fileId)
				.orElseThrow(
						()->new com.dxc.FileUpdate.exception.FileNotFoundException(
								"File Not found with ID"+fileId
								)
						);
	}
	
}
