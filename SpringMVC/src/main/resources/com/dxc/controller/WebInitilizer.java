package com.dxc.controller;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class WebInitilizer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {MVCConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		// Replaces MainServlet-servlet.xml
		return new Class[] {ControllerConfig.class};
//		return null;
	}

	@Override
	protected String[] getServletMappings() {
		// Replaces web.xml <url-pattern>/</url-pattern>
		return new String[] { "/" };
	}

}
