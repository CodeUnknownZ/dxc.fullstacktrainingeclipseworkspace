package com.dxc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dxc.service.AddService;

@Controller
public class AddController {

//	public ModelAndView add(HttpServletRequest request, HttpServletResponse response) { ... }
	
	@RequestMapping("/add")
	public ModelAndView add(@RequestParam("text1") int x, @RequestParam("text2")int y) {
		// RequestParam Annotation to assign parameter values into variables
		AddService as = new AddService();
		int z = as.add(x, y);
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Display");
		modelAndView.addObject("result",z);
		
		return modelAndView;
	}
}
