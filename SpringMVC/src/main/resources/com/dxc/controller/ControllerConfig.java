package com.dxc.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


// Annotations Acting as MainServlet-servlet.xml
// <ctx:component-scan base-package="com.dxc.controller"> </ctx:component-scan> ?

@Configuration
@ComponentScan({"com.dxc.controller"})
public class ControllerConfig {
	@Bean
	public InternalResourceViewResolver viewResolver() {
		InternalResourceViewResolver ivr = new InternalResourceViewResolver();
		
		// Will read from webapp
		ivr.setPrefix("/");
		
		// So that ModelAndView.setViewName("FolderPath/FileNameWithoutExtension");
		ivr.setSuffix(".jsp");
		return ivr;
	}
}
