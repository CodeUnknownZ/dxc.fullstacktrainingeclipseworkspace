<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Display.jsp</title>
</head>
<body>
	Result is :
		Expression - ${result}
		Scriptlet - <%= request.getAttribute("result") %>
</body>
</html>