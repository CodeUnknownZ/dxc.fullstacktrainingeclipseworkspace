package com.dxc.controller;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;

import com.dxc.entity.Employee;
@SuppressWarnings({ "unused", "deprecation" })
public class App {
//	public static void main(String[] args) {
//		// Lazy Loading Mechanism
//		BeanFactory beanFactory = new XmlBeanFactory(new ClassPathXmlApplicationContext("/applicationContext.xml"));
//		Employee employee = (Employee) beanFactory.getBean("employeebean");
//		System.out.println(employee);
//	}

	public static void main(String[] args) {
		// Eager Loading Mechanism
		ApplicationContext appContext = new ClassPathXmlApplicationContext("/applicationContext.xml");
		Employee employee = (Employee) appContext.getBean("employeebean");
		employee.setName("Tina");
		employee.setAddress("USA");
		employee.setSalary(5000);
		System.out.println(employee.getName());
		System.out.println(employee.getSalary());
		System.out.println(employee.getAddress());

		employee = (Employee) appContext.getBean("employeebean");
		System.out.println(employee.getName());
		System.out.println(employee.getSalary());
		System.out.println(employee.getAddress());

	}
}
