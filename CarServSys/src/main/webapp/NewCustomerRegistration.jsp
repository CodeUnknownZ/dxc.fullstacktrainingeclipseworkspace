<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New Customer Registration</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<form action="/CarServSys/NewCustomerRegistration">
    	<p>New Customer Registration</p>
    	<p>Please enter your details below.</p>
    	<br>
        <table>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td>Username</td>
                <td><input type="text" name="username"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="password"></td>
            </tr>
            <tr>
                <td>Confirm Password</td>
                <td><input type="text" name="confirmPassword"></td>
            </tr>
           	<tr>
                <td>Email</td>
                <td><input type="text" name="email"></td>
            </tr>
            <tr>
            	<td></td>
                <td><input type="submit" name="register"></td>
            </tr>
        </table>

    </form>
</body>
</html>