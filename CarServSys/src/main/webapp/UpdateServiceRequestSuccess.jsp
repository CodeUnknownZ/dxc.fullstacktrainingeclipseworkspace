<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update Success</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<p>${username}, you have successfully updated ${servReqNo}</p>
	<br>
	<a href="/CarServSys/AdminHome.jsp">Click here to go to the admin home page </a>
</body>
</html>