<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New Service Request</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<form action="/CarServSys/NewServiceRequest">
		<p>Dear ${username}, what service would you like for your car.</p>
    	<br>
    	
        <table>
            <tr>
                <td>Car Registry Number</td>
                <td><input type="text" name="carRegNo"></td>
            </tr>
            <tr>
                <td>Type of service required</td>
                <td><input type="text" name="servDesc"></td>
            </tr>
            
            <tr>
            	<td></td>
                <td><input type="submit" name="register"></td>
            </tr>
        </table>

    </form>
</body>
</html>