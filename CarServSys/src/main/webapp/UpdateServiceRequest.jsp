<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Update Service Request</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<form action="/CarServSys/UpdateServiceRequest">
		<p>Dear ${username},</p>
		<p>Please enter your Service Request Number below.</p>
		<br>
		<table>
			<tr>
				<td>Service Request Number</td>
				<td><input type="text" name="servReqNo"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="register"></td>
			</tr>
		</table>
	</form>
</body>
</html>