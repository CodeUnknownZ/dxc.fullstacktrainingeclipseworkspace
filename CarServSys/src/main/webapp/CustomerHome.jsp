<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Customer Page</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<p>Welcome ${username}, <br>how can we help you today.</p>
	<br>
	<a href="/CarServSys/AddCarDetails.jsp">Add car details</a>
	<br>
	<a href="/CarServSys/NewServiceRequest.jsp">Create Service Request</a>
</body>
</html>