<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Add Car info</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<form action="/CarServSys/AddCarInfo">
    	<p>Hello ${username},</p>
    	<p>Please enter your car details below.</p>
    	<br>
    	<!--  <input type="text" name="username" hidden=true value=${username}> -->
        <table>
            <tr>
                <td>Model</td>
                <td><input type="text" name="carModel"></td>
            </tr>
            <tr>
                <td>Type</td>
                <td><input type="text" name="carType"></td>
            </tr>
            <tr>
                <td>Car Registry Number</td>
                <td><input type="text" name="carRegNo"></td>
            </tr>
            <tr>
            	<td></td>
                <td><input type="submit" name="register"></td>
            </tr>
        </table>

    </form>
</body>
</html>