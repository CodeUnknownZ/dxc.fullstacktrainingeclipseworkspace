<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Error</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<h1>Please Try Again</h1>
	<script type="text/javascript">
		setTimeout(function(){
			window.location.href = window.location.origin + "/CarServSys/";
		}, 1000); 
	</script>
</body>
</html>