<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<p> ${username}, you have successfully created a new ServiceRequest for car with registration number : ${carRegNo} </p><br>
	<p> Click <a href="/CarServSys/CustomerHome.jsp">here</a> to go to the login page </p>
</body>
</html>