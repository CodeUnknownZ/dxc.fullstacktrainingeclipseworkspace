<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Admin Page</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<p>WELCOME ${username},</p>
	<br>
	<a href="/CarServSys/ViewCustomerList"> View Customer List </a>
	<br>
	<a href="/CarServSys/ViewServiceRequestList"> View Pending Request </a>
	<br>
	<a href="/CarServSys/UpdateServiceRequest.jsp"> Update Status </a>
	<br>
</body>
</html>