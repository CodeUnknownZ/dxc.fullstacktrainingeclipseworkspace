<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Admin Login Page</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
    <form action="/CarServSys/AdminLogin">
    	<p>Admin Login Page</p>
    	<br>
        <table>
            <tr>
                <td>Username</td>
                <td><input type="text" name="username"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="text" name="password"></td>
            </tr>
            <tr>
            	<td></td>
                <td><input type="submit" name="Login"></td>
            </tr>
        </table>

    </form>
</body>
</html>