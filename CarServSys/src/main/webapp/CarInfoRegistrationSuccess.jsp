<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Success</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<p>Your car with registration number: ${carRegNo} has been added to the list.</p>
	<a href="/CarServSys/CustomerHome.jsp"> Click here to return to home page</a>
</body>
</html>