package com.dxc.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerModel {
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	private String name;
	private String email;
	private String username;
	private String password;
	private String confirmPassword;
	private final int isAdmin = 0;
	private CarModel car = null;
	private ServiceRequestModel SR = null;
	
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	public CustomerModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver Loaded Successfully");
			
			this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample_dxc","root","root");
			System.out.println("Connection Established Successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean checkConfirmPassword() {
		return this.confirmPassword.equals(this.password);
	}
	
	public boolean registerCustomer() {
		try {
			// For carsys_users table
			this.pstmt = this.con.prepareStatement("INSERT INTO carsys_users (username, password, isAdmin) VALUES (?,?,?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.password);
			this.pstmt.setInt(3, this.isAdmin);
			
			System.out.println(this.pstmt.toString());
			
			int rows = this.pstmt.executeUpdate();
			
			// For carsys_customer_details table
			this.pstmt = this.con.prepareStatement("INSERT INTO carsys_customer_details (username, name, email) VALUES (?,?,?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.name);
			this.pstmt.setString(3, this.email);
			
			System.out.println(this.pstmt.toString());
			
			rows += this.pstmt.executeUpdate();
			
			return rows==2;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean verifyLogin() {
		try {
			this.pstmt = this.con.prepareStatement("SELECT * FROM carsys_users WHERE (username=? AND password=? AND isAdmin=?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.password);
			this.pstmt.setInt(3, this.isAdmin);
			
			System.out.println(this.pstmt.toString());
			
			
			this.rs = this.pstmt.executeQuery();
			return rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int registerCar(String carModel, String carType, String carRegNo) {
		this.car = new CarModel(this.con);
		
		this.car.setUsername(this.username);
		this.car.setCarModel(carModel);
		this.car.setCarType(carType);
		this.car.setCarRegNo(carRegNo);
		this.car.setServReqNo(null);
		this.car.setServStatus(null);
		
		int rows = this.car.addCarDetails();
		return rows;
	}
	
	public boolean newServiceRequest(String carRegNo, String servDesc) {
		this.SR = new ServiceRequestModel(this.con);
		String SRNum = this.SR.generateSRNumber();
		int rows = 0;
		try {
			this.pstmt = this.con.prepareStatement("UPDATE carsys_car_details SET servReqNo=? WHERE carRegNo=?;");
			this.pstmt.setString(1, SRNum);
			this.pstmt.setString(2, carRegNo);
			
			System.out.println(this.pstmt.toString());
			
			rows += this.pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.SR.setServDesc(servDesc);
		this.SR.setServReqNo(SRNum);
		this.SR.setServStatus("inProgress");
		
		rows += this.SR.newRequest();
		return rows==2;
	}
	
}
