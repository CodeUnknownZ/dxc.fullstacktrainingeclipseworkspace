package com.dxc.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class ServiceRequestModel {
	private String servReqNo;
	private String servStatus;
	private String servDesc;
	private String carRegNo;
	private String email;
	private String name;

	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	public String getServReqNo() {
		return servReqNo;
	}

	public void setServReqNo(String servReqNo) {
		this.servReqNo = servReqNo;
	}

	public String getServStatus() {
		return servStatus;
	}

	public void setServStatus(String servStatus) {
		this.servStatus = servStatus;
	}

	public String getServDesc() {
		return servDesc;
	}

	public void setServDesc(String servDesc) {
		this.servDesc = servDesc;
	}

	public String getCarRegNo() {
		return carRegNo;
	}

	public void setCarRegNo(String carRegNo) {
		this.carRegNo = carRegNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ServiceRequestModel(Connection con) {
		this.con = con;
	}

	public ServiceRequestModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver Loaded Successfully");

			this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample_dxc", "root", "root");
			System.out.println("Connection Established Successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int newRequest() {
		try {
			this.pstmt = this.con.prepareStatement(
					"INSERT INTO carsys_service_requests (servReqNo, servStatus, servDesc) VALUES (?,?,?)");
			this.pstmt.setString(1, this.servReqNo);
			this.pstmt.setString(2, this.servStatus);
			this.pstmt.setString(3, this.servDesc);

			System.out.println(this.pstmt.toString());

			return this.pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String generateSRNumber() {
		int number = new Random().nextInt(9999);
		SimpleDateFormat formatter = new SimpleDateFormat("ddMM-HHmm-");
		Date date = new Date();
		System.out.println();
		return "SR" + formatter.format(date) + number;
	}

	public ArrayList<ServiceRequestModel> viewPendingRequests() {
		try {
			this.pstmt = this.con.prepareStatement(
					"SELECT \r\n" + "	carsys_car_details.servReqNo,\r\n" + "	carsys_service_requests.servStatus,\r\n"
							+ "	carsys_service_requests.servDesc,\r\n" + "	carsys_car_details.carRegNo,\r\n"
							+ "	carsys_customer_details.name,\r\n" + "	carsys_customer_details.email\r\n"
							+ "FROM carsys_service_requests\r\n" + "LEFT OUTER JOIN carsys_car_details\r\n"
							+ "  ON carsys_service_requests.servReqNo = carsys_car_details.servReqNo\r\n"
							+ "LEFT OUTER JOIN carsys_customer_details\r\n"
							+ "  ON carsys_car_details.username = carsys_customer_details.username;");

			this.rs = this.pstmt.executeQuery();

			ArrayList<ServiceRequestModel> servRequestList = new ArrayList<ServiceRequestModel>();
			while (rs.next()) {
				ServiceRequestModel servReqMdl = new ServiceRequestModel(this.con);
				servReqMdl.setServReqNo(this.rs.getString(1));
				servReqMdl.setServStatus(this.rs.getString(2));
				servReqMdl.setServDesc(this.rs.getString(3));
				servReqMdl.setCarRegNo(this.rs.getString(4));
				servReqMdl.setName(this.rs.getString(5));
				servReqMdl.setEmail(this.rs.getString(6));

				servRequestList.add(servReqMdl);
			}
			return servRequestList;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean updateStatus(String servReqNo) {
		try {
			this.pstmt = this.con.prepareStatement(
					"UPDATE carsys_service_requests\r\n" + "SET servStatus=?\r\n" + "WHERE servReqNo=?");

			this.pstmt.setString(1, "completed");
			this.pstmt.setString(2, servReqNo);

			return this.pstmt.executeUpdate() == 1;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

}
