package com.dxc.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdminModel {
	private String username;
	private String password;
	private final int isAdmin = 1;

	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasssword() {
		return password;
	}

	public void setPasssword(String passsword) {
		this.password = passsword;
	}

	public boolean verifyLogin() {
		try {
			this.pstmt = this.con
					.prepareStatement("SELECT * FROM carsys_users WHERE (username=? AND password=? AND isAdmin=?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.password);
			this.pstmt.setInt(3, this.isAdmin);

			System.out.println(this.pstmt.toString());

			this.rs = this.pstmt.executeQuery();
			return rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public AdminModel(String username, String passsword) {
		this();
		this.username = username;
		this.password = passsword;
	}

	public AdminModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver Loaded Successfully");

			this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample_dxc", "root", "root");
			System.out.println("Connection Established Successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AdminModel(Connection con) {
		this.con = con;
	}

	public ArrayList<CustomerModel> getCustomerList() {
		try {
			// Joins carsys_users and carsys_customer_details on username as key
			String statement = "SELECT carsys_users.username, carsys_customer_details.name, carsys_customer_details.email "
					+ "FROM carsys_users " + "LEFT OUTER JOIN carsys_customer_details "
					+ "ON carsys_users.username = carsys_customer_details.username;";
			this.pstmt = this.con.prepareStatement(statement);
			this.rs = this.pstmt.executeQuery();

			// Save details from query to customer models
			ArrayList<CustomerModel> cstList = new ArrayList<CustomerModel>();
			while (this.rs.next()) {
				CustomerModel cstMdl = new CustomerModel();

				cstMdl.setUsername(this.rs.getString(1));
				cstMdl.setName(this.rs.getString(2));
				cstMdl.setEmail(this.rs.getString(3));

				cstList.add(cstMdl);
			}
			return cstList;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
