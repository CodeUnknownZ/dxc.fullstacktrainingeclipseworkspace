package com.dxc.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CarModel {
	private String username;
	private String carModel;
	private String carType;
	private String carRegNo;
	private String servReqNo = null;
	private String servStatus = null;
	
	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	int updatedRows = 0;
	
	public CarModel(Connection con) {
		this.con = con;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCarModel() {
		return carModel;
	}

	public void setCarModel(String carModel) {
		this.carModel = carModel;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getCarRegNo() {
		return carRegNo;
	}

	public void setCarRegNo(String carRegNo) {
		this.carRegNo = carRegNo;
	}

	public String getServReqNo() {
		return servReqNo;
	}

	public void setServReqNo(String servReqNo) {
		this.servReqNo = servReqNo;
	}

	public String getServStatus() {
		return servStatus;
	}

	public void setServStatus(String servStatus) {
		this.servStatus = servStatus;
	}

	public CarModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver Loaded Successfully");
			
			this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample_dxc","root","root");
			System.out.println("Connection Established Successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int addCarDetails() {
		try {
			// For carsys_car_details table
			this.pstmt = this.con.prepareStatement("INSERT INTO carsys_car_details (username, carModel, carType, carRegNo, servReqNo) VALUES (?,?,?,?,?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.carModel);
			this.pstmt.setString(3, this.carType);
			this.pstmt.setString(4, this.carRegNo);
			this.pstmt.setString(5, this.servReqNo);
			
			System.out.println(this.pstmt.toString());
			
			this.updatedRows = this.pstmt.executeUpdate();
			return this.updatedRows;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
