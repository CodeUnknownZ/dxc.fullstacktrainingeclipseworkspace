package com.dxc.controllers;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.AdminModel;
import com.dxc.models.CustomerModel;

public class ViewCustomerList extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		AdminModel adMdl = new AdminModel();

		ArrayList<CustomerModel> cstList = adMdl.getCustomerList();

		HttpSession sess = req.getSession(true);
		sess.setAttribute("customer_list", cstList);

		if (cstList == null) {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}
		resp.sendRedirect("/CarServSys/ViewCustomerList.jsp");
	}
}
