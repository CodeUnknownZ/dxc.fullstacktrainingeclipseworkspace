package com.dxc.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.CustomerModel;

public class AddCarInfo extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerModel cstMdl = new CustomerModel();

		HttpSession sess = req.getSession(true);
		cstMdl.setUsername((String) sess.getAttribute("username"));
		int rows = cstMdl.registerCar(req.getParameter("carModel"), req.getParameter("carType"),
				req.getParameter("carRegNo"));

		if (rows > 0) {
			sess.setAttribute("carRegNo", req.getParameter("carRegNo"));
			resp.sendRedirect("/CarServSys/CarInfoRegistrationSuccess.jsp");
		} else {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}
	}
}
