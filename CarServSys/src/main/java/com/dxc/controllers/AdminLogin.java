package com.dxc.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.AdminModel;

public class AdminLogin extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		AdminModel al = new AdminModel(req.getParameter("username"), req.getParameter("password"));
		if (al.verifyLogin()) {
			HttpSession sess = req.getSession(true);
			sess.setAttribute("username", req.getParameter("username"));
			resp.sendRedirect("/CarServSys/AdminHome.jsp");
		} else {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}

	}
}
