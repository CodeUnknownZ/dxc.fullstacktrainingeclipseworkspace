package com.dxc.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.CustomerModel;

public class CustomerLogin extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerModel cstMdl = new CustomerModel();

		cstMdl.setUsername(req.getParameter("username"));
		cstMdl.setPassword(req.getParameter("password"));

		if (cstMdl.verifyLogin()) {
			HttpSession sess = req.getSession(true);
			sess.setAttribute("username", req.getParameter("username"));
			resp.sendRedirect("/CarServSys/CustomerHome.jsp");
		} else {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}

	}

}
