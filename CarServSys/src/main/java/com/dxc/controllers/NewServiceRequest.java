package com.dxc.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.CustomerModel;

public class NewServiceRequest extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerModel cstMdl = new CustomerModel();

		if (cstMdl.newServiceRequest(req.getParameter("carRegNo"), req.getParameter("servDesc"))) {
			HttpSession sess = req.getSession(true);
			sess.setAttribute("carRegNo", req.getParameter("carRegNo"));
			resp.sendRedirect("/CarServSys/NewServiceRequestSuccess.jsp");
		} else {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}
	}

}
