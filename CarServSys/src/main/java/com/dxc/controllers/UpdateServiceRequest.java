package com.dxc.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.ServiceRequestModel;

/**
 * Servlet implementation class UpdateServiceRequest
 */
public class UpdateServiceRequest extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServiceRequestModel servReqMdl = new ServiceRequestModel();

		if (servReqMdl.updateStatus(req.getParameter("servReqNo"))) {
			HttpSession sess = req.getSession(true);
			sess.setAttribute("servReqNo", req.getParameter("servReqNo"));
			resp.sendRedirect("/CarServSys/UpdateServiceRequestSuccess.jsp");
		} else {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}

	}

}
