package com.dxc.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.ServiceRequestModel;

/**
 * Servlet implementation class ViewServiceRequestList
 */
public class ViewServiceRequestList extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ServiceRequestModel SRMdl = new ServiceRequestModel();

		ArrayList<ServiceRequestModel> servRequestList = SRMdl.viewPendingRequests();
		PrintWriter pw = resp.getWriter();

		Object[] servList = new Object[servRequestList.size()];

		HttpSession sess = req.getSession(true);
		for (int i = 0; i < servList.length; i++) {
			ServiceRequestModel item = servRequestList.get(i);
			sess.setAttribute("Service Requests", item);
		}

		for (ServiceRequestModel item : servRequestList) {
			String ntT = " \t|\t ";
			pw.println(item.getServReqNo() + ntT + item.getServStatus() + ntT + item.getServDesc() + ntT
					+ item.getCarRegNo() + ntT + item.getName() + ntT + item.getEmail());
		}
	}
}
