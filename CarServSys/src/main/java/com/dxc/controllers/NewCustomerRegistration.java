package com.dxc.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.models.CustomerModel;

public class NewCustomerRegistration extends HttpServlet {

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		CustomerModel cstMdl = new CustomerModel();

		cstMdl.setName(req.getParameter("name"));
		cstMdl.setUsername(req.getParameter("username"));
		cstMdl.setPassword(req.getParameter("password"));
		cstMdl.setConfirmPassword(req.getParameter("confirmPassword"));
		cstMdl.setEmail(req.getParameter("email"));
		if (!cstMdl.checkConfirmPassword()) {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}
		boolean success = cstMdl.registerCustomer();

		HttpSession sess = req.getSession(true);
		sess.setAttribute("name", req.getParameter("name"));

		if (success) {
			resp.sendRedirect("/CarServSys/NewCustomerRegistrationSuccess.jsp");
		} else {
			resp.sendRedirect("/CarServSys/Error.jsp");
		}
	}

}
