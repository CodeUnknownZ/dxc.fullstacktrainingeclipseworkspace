## Using Repo as Cloud save

### Save Local Files to git

        git add .
        git commit -m "Save local changes"
        git push -u origin main

### Resources

-   https://repo1.maven.org/maven2/archetype-catalog.xml ([downloaded file](archetype-catalog.xml))
-   https://www.tutorialspoint.com/hibernate/index.htm
-   https://www.javatpoint.com/hibernate-one-to-many-mapping-using-annotation-example

### Dependencies

-   https://mvnrepository.com/artifact/org.springframework/spring-context/5.2.22.RELEASE

        <!-- https://mvnrepository.com/artifact/org.springframework/spring-context -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.2.22.RELEASE</version>
        </dependency>

-   https://mvnrepository.com/artifact/org.springframework/spring-orm/5.2.22.RELEASE

        <!-- https://mvnrepository.com/artifact/org.springframework/spring-orm -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-orm</artifactId>
            <version>5.2.22.RELEASE</version>
        </dependency>

-   https://mvnrepository.com/artifact/org.springframework/spring-beans/5.2.22.RELEASE

        <!-- https://mvnrepository.com/artifact/org.springframework/spring-beans -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>5.2.22.RELEASE</version>
        </dependency>

-   https://mvnrepository.com/artifact/org.hibernate/hibernate-core/5.6.10.Final

        <!-- https://mvnrepository.com/artifact/org.hibernate/hibernate-core -->
        <dependency>
        	<groupId>org.hibernate</groupId>
        	<artifactId>hibernate-core</artifactId>
        	<version>5.6.10.Final</version>
        </dependency>

-   https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.30

        <!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
        <dependency>
        	<groupId>mysql</groupId>
        	<artifactId>mysql-connector-java</artifactId>
        	<version>8.0.30</version>
        </dependency>

-   https://mvnrepository.com/artifact/javax.persistence/javax.persistence-api

        <!-- https://mvnrepository.com/artifact/javax.persistence/javax.persistence-api -->
        <dependency>
        	<groupId>javax.persistence</groupId>
        	<artifactId>javax.persistence-api</artifactId>
        	<version>2.2</version>
        </dependency>

-   https://mvnrepository.com/artifact/org.springframework/spring-webmvc/5.2.22.RELEASE

        <!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
        <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>5.2.22.RELEASE</version>
        </dependency>

-   https://mvnrepository.com/artifact/javax.servlet/jstl/1.2

        <!-- https://mvnrepository.com/artifact/javax.servlet/jstl -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>

-   https://mvnrepository.com/artifact/org.projectlombok/lombok/1.18.12

        <!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.12</version>
            <scope>provided</scope>
        </dependency>

