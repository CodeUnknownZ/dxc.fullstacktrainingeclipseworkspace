package com.dxc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogApplication {

	public static void main(String[] args) {
		StringBuffer s1 = new StringBuffer("Quiz");
		StringBuffer s2 = s1.reverse();
		System.out.println(s2);
		SpringApplication.run(BlogApplication.class, args);
	}

}
