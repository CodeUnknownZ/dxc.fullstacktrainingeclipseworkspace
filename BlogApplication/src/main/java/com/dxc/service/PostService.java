package com.dxc.service;

import org.springframework.stereotype.Service;

import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;

@Service
public interface PostService {
	PostDTO createPost(PostDTO postDTO);

	PostResponse getAppPosts(int pgNo, int pgSize, String sortBy, String sortDirection);

	PostDTO getPostByID(long id);

	PostDTO updatePost(PostDTO postDTO, long id);

	void deletePostByID(long id);
}
