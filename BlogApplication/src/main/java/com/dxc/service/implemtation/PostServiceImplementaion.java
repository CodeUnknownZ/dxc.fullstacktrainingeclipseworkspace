package com.dxc.service.implemtation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.dxc.entity.Post;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;
import com.dxc.repository.PostRepository;
import com.dxc.service.PostService;

@Service
public class PostServiceImplementaion implements PostService {
	@Autowired
	PostRepository postRepository;

	// Convert Entity to DTO
	private PostDTO mapToDTO(Post post) {
		PostDTO postDTO = new PostDTO();
		postDTO.setId(post.getId());
		postDTO.setTitle(post.getTitle());
		postDTO.setDescription(post.getDescription());
		postDTO.setContents(post.getContents());
		return postDTO;
	}

	// Convert DTO to Entity
	private Post mapToEntity(PostDTO postDTO) {
		Post post = new Post();
		post.setId(postDTO.getId());
		post.setTitle(postDTO.getTitle());
		post.setDescription(postDTO.getDescription());
		post.setContents(postDTO.getContents());
		return post;
	}

	@Override
	public PostDTO createPost(PostDTO postDTO) {
		return mapToDTO(postRepository.save(mapToEntity(postDTO)));
	}

	@Override
	public PostResponse getAppPosts(int pgNo, int pgSize, String sortBy, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();
		Pageable pagable = PageRequest.of(pgNo, pgSize, sort);
		Page<Post> posts = postRepository.findAll(pagable);

		List<Post> listOfPosts = posts.getContent();
		List<PostDTO> contents = listOfPosts.stream().map(post -> mapToDTO(post)).collect(Collectors.toList());

		PostResponse postResponse = new PostResponse();
		postResponse.setContent(contents);
		postResponse.setPageNo(posts.getNumber());
		postResponse.setPageSize(posts.getSize());
		postResponse.setTotalEmlement(posts.getTotalElements());
		postResponse.setTotalPages(posts.getTotalPages());
		postResponse.setLast(posts.isLast());
		return postResponse;
	}

	@Override
	public PostDTO getPostByID(long id) {
		Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("POST", "id", id));
		return mapToDTO(post);
	}

	@Override
	public PostDTO updatePost(PostDTO postDTO, long id) {
		Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("POST", "id", id));
		post.setTitle(postDTO.getTitle());
		post.setDescription(postDTO.getTitle());
		post.setContents(postDTO.getContents());
		return mapToDTO(postRepository.save(post));
	}

	@Override
	public void deletePostByID(long id) {
		Post post = postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("POST", "id", id));
		postRepository.delete(post);
	}

}
