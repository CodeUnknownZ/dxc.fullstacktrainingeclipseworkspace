package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;
import com.dxc.service.PostService;
import com.dxc.utils.AppConstants;

@RestController
@RequestMapping("api/posts")
public class PostController {
	@Autowired
	private PostService postService;

	// Create blog post
	@PostMapping
	public ResponseEntity<PostDTO> createPost(@RequestBody PostDTO postDTO) {
		return new ResponseEntity<PostDTO>(postService.createPost(postDTO), HttpStatus.CREATED);
	}

	@GetMapping
	public PostResponse getAllBlobs(
			@RequestParam(value = "pgNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pgNo,
			@RequestParam(value = "pgSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pgSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDirection", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDirection) {
		return postService.getAppPosts(pgNo, pgSize, sortBy, sortDirection);
	}

	@GetMapping("/{id}")
	public ResponseEntity<PostDTO> getPostByID(@PathVariable(name = "id") long id) {
		return new ResponseEntity<PostDTO>(postService.getPostByID(id), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<PostDTO> updatePost(@RequestBody PostDTO postDTO, @PathVariable(name = "id") long id) {
		return new ResponseEntity<PostDTO>(postService.updatePost(postDTO, id), HttpStatus.OK);
	}

	
	@DeleteMapping
	public ResponseEntity<String> deletePostByID(@PathVariable(name = "id") long id) {
		postService.deletePostByID(id);
		return new ResponseEntity<String>("Post entity with id: " + id + "deleted successfully.", HttpStatus.OK);
	}
}
