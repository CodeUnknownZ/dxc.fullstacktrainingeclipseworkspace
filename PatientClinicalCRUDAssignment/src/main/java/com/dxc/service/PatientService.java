package com.dxc.service;

import java.util.ArrayList;

import com.dxc.entities.ClinicalData;
import com.dxc.entities.Patient;

public interface PatientService {
	void deletePatient(int id);

	Patient updatePatient(int id, Patient newPatientData);

	Patient getPatientByID(int id);

	int presistPatient(Patient patient);

	Patient createPatient(int age, String firstName, String lastName, ArrayList<ClinicalData> clinicalDataList);
	
	ArrayList<Patient> getPatientList();
}
