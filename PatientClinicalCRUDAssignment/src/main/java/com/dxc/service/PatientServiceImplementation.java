package com.dxc.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.RollbackException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dxc.entities.ClinicalData;
import com.dxc.entities.Patient;
import com.dxc.factorydesign.HibernateFactory;

public class PatientServiceImplementation implements PatientService {

	SessionFactory factory;
	Session session;
	Transaction transaction = null;

	public PatientServiceImplementation() {
		this.factory = HibernateFactory.getFactoryObject();
	}

	private void startTransaction() {
		try {
			this.session = this.factory.openSession();
			this.transaction = this.session.beginTransaction();
		} catch (HibernateException e) {
			System.err.println("Error in opening the session............");
			e.printStackTrace();
		}
	}

	private void endTransaction() {
		try {
			this.transaction.commit();
		} catch (HibernateException | RollbackException e) {
			if (this.transaction != null) {
				this.transaction.rollback();
			}
			System.err.println("Error Closing Session/On Commit...");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.session.close();
			this.transaction = null;
		}
	}

	@Override
	public void deletePatient(int id) {
		Patient patient = getPatientByID(id);

		this.startTransaction();
		this.session.delete(patient);
		this.endTransaction();

		patient.printPatientData();
		System.out.println("Above patient details has been removed from the records.");
	}

	@Override
	public Patient updatePatient(int id, Patient newPatientData) {
		Patient oldPatientData = getPatientByID(id).printPatientData();
		if (newPatientData.getAge() != 0) {
			oldPatientData.setAge(newPatientData.getAge());
		}

		if (newPatientData.getFirstName() != null) {
			oldPatientData.setFirstName(newPatientData.getFirstName());
		}

		if (newPatientData.getLastName() != null) {
			oldPatientData.setLastName(newPatientData.getLastName());
		}

		if (newPatientData.getClinicalData() != null) {
			if (!newPatientData.getClinicalData().isEmpty()) {
				oldPatientData.setClinicalData(newPatientData.getClinicalData());
			}
		}

		startTransaction();
		this.session.update(oldPatientData);
		endTransaction();

		return oldPatientData;
	}

	@Override
	public Patient getPatientByID(int id) {
		startTransaction();
		Patient patient = (Patient) this.session.get(Patient.class, id);
		endTransaction();
		if (patient == null) {
			System.err.println("Unable to find record of patient with ID of " + id);
		}
		return patient;
	}

	@Override
	public int presistPatient(Patient patient) {
		startTransaction();
		Integer id = (Integer) this.session.save(patient);
		endTransaction();
		return id;
	}

	@Override
	public Patient createPatient(int age, String firstName, String lastName, ArrayList<ClinicalData> clinicalDataList) {
		Patient patient = new Patient();
		patient.setAge(age);
		patient.setFirstName(firstName);
		patient.setLastName(lastName);
		patient.setClinicalData(clinicalDataList);
		return patient;
	}

	@Override
	public ArrayList<Patient> getPatientList() {
		startTransaction();
		@SuppressWarnings("unchecked")
		ArrayList<Patient> patients = (ArrayList<Patient>) this.session.createQuery("FROM Patient").list();
		endTransaction();
		
		for (Iterator<Patient> iterator = patients.iterator(); iterator.hasNext();) {
			iterator.next().printPatientData();
		}
		
		return patients;
	}

}
