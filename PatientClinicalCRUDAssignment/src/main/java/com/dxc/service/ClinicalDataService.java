package com.dxc.service;

import java.util.ArrayList;
import com.dxc.entities.ClinicalData;

public interface ClinicalDataService {
	ClinicalData createClinicalData(String componentName, String componentValue);
	
	ArrayList<ClinicalData> getClinicalDataByPatientID(int id);
}
