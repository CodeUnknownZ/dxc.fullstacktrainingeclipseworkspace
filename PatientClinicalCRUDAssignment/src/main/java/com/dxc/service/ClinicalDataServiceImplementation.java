package com.dxc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;

import org.hibernate.*;
import org.hibernate.annotations.NamedQuery;

import com.dxc.entities.ClinicalData;
import com.dxc.entities.Patient;
import com.dxc.factorydesign.HibernateFactory;

public class ClinicalDataServiceImplementation implements ClinicalDataService {
	SessionFactory factory;
	Session session;
	Transaction transaction = null;

	public ClinicalDataServiceImplementation() {
		this.factory = HibernateFactory.getFactoryObject();
	}

	private void startTransaction() {
		try {
			this.session = this.factory.openSession();
			this.transaction = this.session.beginTransaction();
		} catch (HibernateException e) {
			System.err.println("Error in opening the session............");
			e.printStackTrace();
		}
	}

	private void endTransaction() {
		try {
			this.transaction.commit();
		} catch (HibernateException | RollbackException e) {
			if (this.transaction != null) {
				this.transaction.rollback();
			}
			System.err.println("Error Closing Session/On Commit...");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.session.close();
			this.transaction = null;
		}
	}

	@Override
	public ClinicalData createClinicalData(String componentName, String componentValue) {
		ClinicalData clinicalData = new ClinicalData();
		clinicalData.setComponentName(componentName);
		clinicalData.setComponentValue(componentValue);
		clinicalData.setMeasuredTime(new Date());
		return clinicalData;
	}
	
	@Override
	public ArrayList<ClinicalData> getClinicalDataByPatientID(int id) {

		startTransaction();
		
		TypedQuery<ClinicalData> query = this.session.getNamedQuery("findClinicalDataByPatientID");
		query.setParameter("id", id);
		ArrayList<ClinicalData> clinicalDataList = (ArrayList<ClinicalData>) query.getResultList();
		
		endTransaction();
		
		for (Iterator<ClinicalData> iterator = clinicalDataList.iterator(); iterator.hasNext();) {
			iterator.next().printClinicalData();
		}

		return null;
	}

}
