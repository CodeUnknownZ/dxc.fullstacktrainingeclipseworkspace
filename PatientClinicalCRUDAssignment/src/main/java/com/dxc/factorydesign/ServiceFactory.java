package com.dxc.factorydesign;

import com.dxc.service.*;

public class ServiceFactory {
	
	public static final PatientServiceImplementation patientServiceImplementation;
	public static final ClinicalDataServiceImplementation clinicalDataServiceImplementation;
	
	static {
		patientServiceImplementation = new PatientServiceImplementation();
		clinicalDataServiceImplementation = new ClinicalDataServiceImplementation();
	}

	public static PatientServiceImplementation getPatientserviceimplementation() {
		return patientServiceImplementation;
	}

	public static ClinicalDataServiceImplementation getClinicaldataserviceimplementation() {
		return clinicalDataServiceImplementation;
	}
	

}
