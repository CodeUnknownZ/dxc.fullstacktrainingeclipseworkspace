package com.dxc.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@NamedQueries(  
	    {  
	        @NamedQuery(  
	        name = "findClinicalDataByPatientID",  
	        query = "FROM Patient JOIN ClinicalData WHERE Patient.id = :id"
	        )  
	    }  
	) 
@Entity
@Table(name = "PatientData")
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "firstName", nullable = false)
	private String firstName;
	@Column(name = "lastName", nullable = false)
	private String lastName;
	@Column(name = "age", nullable = false)
	private int age;
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "patient_id")
	@OrderColumn(name = "serial")
//	@JoinTable(
//		    name = "patient_clinical_data_assosiation",
//		    joinColumns = @JoinColumn(name = "patient_id"),
//		    inverseJoinColumns = @JoinColumn(name = "clinical_data_id", referencedColumnName="id")
//	)
	private List<ClinicalData> clinicalData;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<ClinicalData> getClinicalData() {
		return clinicalData;
	}

	public void setClinicalData(List<ClinicalData> clinicalData) {
		this.clinicalData = clinicalData;
	}

	public void addClinicalData(ClinicalData clinicalData) {
		if (this.clinicalData.isEmpty()) {
			this.clinicalData = new ArrayList<ClinicalData>();
		}
		this.clinicalData.add(clinicalData);
	}

	public Patient printPatientData() {
		System.out.println("ID: " + this.id + " |\tFirstName: " + this.firstName + " |\tLastName: " + this.lastName
				+ " |\tAge :" + this.age);
		return this;
	}
}
