package com.dxc.entities;

import java.util.Date;

import javax.persistence.*;
 
@Entity
@Table(name = "ClinicalData")
public class ClinicalData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "componentName", nullable = false)
	private String componentName;
	@Column(name = "componentValue", nullable = false)
	private String componentValue;
	@Column(name = "measuredTime", nullable = false)
	private Date measuredTime;

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getComponentValue() {
		return componentValue;
	}

	public void setComponentValue(String componentValue) {
		this.componentValue = componentValue;
	}

	public Date getMeasuredTime() {
		return measuredTime;
	}

	public void setMeasuredTime(Date measuredTime) {
		this.measuredTime = measuredTime;
	}

	public ClinicalData printClinicalData() {
		System.out.println("Name: " + this.componentName + " |\tValue: " + this.componentValue + " |\tTimeMeasured: " + this.measuredTime);
		return this;
	}
}
