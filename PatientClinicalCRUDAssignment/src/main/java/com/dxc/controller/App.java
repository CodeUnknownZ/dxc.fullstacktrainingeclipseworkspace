package com.dxc.controller;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.dxc.factorydesign.ServiceFactory;
import com.dxc.entities.*;
import com.dxc.service.*;

@SuppressWarnings("unused")
public class App {

	static Session session = null;
	static Transaction transaction = null;

	public static void main(String[] args) {
		PatientService patientService = ServiceFactory.getPatientserviceimplementation();
		ClinicalDataService clinicalDataService = ServiceFactory.getClinicaldataserviceimplementation();
		// CREATE PATIENT
//		ArrayList<ClinicalData> clinicalDataList = new ArrayList<ClinicalData>();
//		clinicalDataList.add(clinicalDataService.createClinicalData("Height", "156"));
//		clinicalDataList.add(clinicalDataService.createClinicalData("Weight", "59"));
//
//		Patient patient = patientService.createPatient(25, "Nurhaikal", "Johari", clinicalDataList);
//		patientService.presistPatient(patient.printPatientData());
//
//		clinicalDataList = new ArrayList<ClinicalData>();
//		clinicalDataList.add(clinicalDataService.createClinicalData("Height", "166"));
//		clinicalDataList.add(clinicalDataService.createClinicalData("Weight", "70"));
//		patient = patientService.createPatient(59, "Joe", "Mana", clinicalDataList);
//		patientService.presistPatient(patient.printPatientData());
//		
//		clinicalDataList = new ArrayList<ClinicalData>();
//		clinicalDataList.add(clinicalDataService.createClinicalData("Height", "176"));
//		clinicalDataList.add(clinicalDataService.createClinicalData("Weight", "69"));
//		patient = patientService.createPatient(29, "Bob", "Chad", clinicalDataList);
//		patientService.presistPatient(patient.printPatientData());

		// GET PATIENT BY ID
//		patientService.getPatientByID(2).printPatientData();

		// GET ALL PATIENT
//		patientService.getPatientList();

		// UPDATE PATIENT DETAILS
//		patientService.updatePatient(3, patientService.createPatient(27, "Ginny", null, null)).printPatientData();

		// DELETE PATIENT
//		patientService.deletePatient(2);
		// TODO ADD CLINICAL DATA

		// GET ALL CLINICAL DATA BY PATIENT ID
		// TODO FIX ERROR Caused by: java.lang.NullPointerException: Cannot read field "value" because "s1" is null
		clinicalDataService.getClinicalDataByPatientID(3);

		// TODO GET CLINICAL DATA BY CLINICAL ID

		// TODO UPDATE CLINICAL DATA

		// TODO DELETE CLINICAL DATA

		// TODO CLI FOR ALL ABOVE FUNCTIONS
	}
}
