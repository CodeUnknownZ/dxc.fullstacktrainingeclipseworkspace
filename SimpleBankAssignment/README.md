# Banking Asignment

## Requirements

### Users
- Admin
- Customer
----------
### Flows

- Admin
	- Login (COMPLETED)
	----------
	After login
	- View Customer (Completed Error?)
	- View Loans (TODO)
	- Update statis of loans (Approve, Reject, Hold) (TODO)
	- Analysis View (TODO)
		- View Loan types - housing, personal, etc
	----------
- Customer
	- Register free $1000 to account (COMPLETED)
	- Login (COMPLETED)
	- Forget password (COMPLETED)
	----------
	After Login
	- Change password (COMPLETED)
	- Check balance (COMPLETED)
	- Deposit money (COMPLETED)
	- Send money (Send to another customer acc)  (TODO)
		- Invalid if insufficient amount
	- Apply Loan (TO START)
		- housing, personal, etc - get user input
	- Past transaction (TODO)
	
