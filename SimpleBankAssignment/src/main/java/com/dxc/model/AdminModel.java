package com.dxc.model;

import java.sql.Connection;
import java.util.ArrayList;

public class AdminModel extends UserModel {

	protected final int isAdmin = 1;

	public AdminModel() {
		super();
	}

	public AdminModel(Connection connection) {
		super(connection);
	}

	public boolean checkIsAdmin() {
		try {
			this.pstmt = this.connection.prepareStatement("SELECT * FROM sbsa_users WHERE (username=? AND isAdmin=?);");

			this.pstmt.setString(1, this.username);
			this.pstmt.setInt(2, isAdmin);

			System.out.println(this.pstmt.toString());

			this.rs = this.pstmt.executeQuery();
			return rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public ArrayList<CustomerModel> getCustomerList() {
		try {
			this.pstmt = this.connection.prepareStatement("SELECT * FROM sbsa_users WHERE isAdmin=0;");
			
			System.out.println(this.pstmt.toString());
			
			this.rs = this.pstmt.executeQuery();
			
			
			ArrayList<CustomerModel> customerModelList = new ArrayList<CustomerModel>();
			while (this.rs.next()) {
				CustomerModel tempCustomerModel = new CustomerModel();
				tempCustomerModel.setUsername(this.rs.getString(1));
				tempCustomerModel.setName(this.rs.getString(3));
				customerModelList.add(tempCustomerModel);
			}
			return customerModelList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}