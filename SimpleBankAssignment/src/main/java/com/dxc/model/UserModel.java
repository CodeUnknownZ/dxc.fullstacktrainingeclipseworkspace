package com.dxc.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

public class UserModel {

	Connection connection = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	protected String username;
	protected String password;
	protected String confirmPassword;

	protected String name;

	protected int isAdmin;
	protected double balance;

	private final double CustRegistrationCredit = 1000.00;
	private final double AdminRegistrationCredit = 99999999999.99;

	public UserModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			this.connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample_dxc", "root", "root");
		} catch (Exception e) {
			System.out.println("JDBC Connection Error Has Occured");
			e.printStackTrace();
		}
	}

	public UserModel(Connection connection) {
		this.connection = connection;
	}

	public Connection getConnection() {
		return connection;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(int isAdmin) {
		this.isAdmin = isAdmin;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public void printUserInfo() {
		System.out.println("Username: "+ this.username);
		System.out.println("Name: "+ this.name);
		System.out.println("Password: "+ this.password);
		System.out.println("Confirm Password: "+ this.confirmPassword);
		System.out.println("Admin ? : "+ this.isAdmin);
		System.out.println("Balance: "+ this.balance);
	}

	public boolean checkConfirmPassword() {
		return this.password.equals(this.confirmPassword);
	}

	public Boolean verifyLogin(String username, String password) {
		this.setUsername(username);
		this.setPassword(password);
		return this.verifyLogin();
	}

	public Boolean verifyLogin() {
		try {
			this.pstmt = this.connection
					.prepareStatement("SELECT * FROM sbsa_users WHERE (username=? AND password=?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.password);

			System.out.println(this.pstmt.toString());

			this.rs = this.pstmt.executeQuery();
			return rs.next();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean registerUser(boolean isAdmin) throws MySQLIntegrityConstraintViolationException {
		if (isAdmin) {
			this.isAdmin = 1;
			this.balance = this.AdminRegistrationCredit;
		} else {
			this.isAdmin = 0;
			this.balance = this.CustRegistrationCredit;
		}

		try {
			this.pstmt = this.connection.prepareStatement(
					"INSERT INTO sbsa_users(username, password, name, balance, isAdmin) VALUES (?, ?, ?, ?, ?);");
			this.pstmt.setString(1, this.username);
			this.pstmt.setString(2, this.password);
			this.pstmt.setString(3, this.name);
			this.pstmt.setDouble(4, this.balance);
			this.pstmt.setInt(5, this.isAdmin);

			System.out.println(this.pstmt.toString());

			return this.pstmt.executeUpdate() == 1;
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updatePassword() throws MySQLIntegrityConstraintViolationException {
		try {
			this.pstmt = this.connection
					.prepareStatement("UPDATE sbsa_users SET password=? WHERE username=? AND name=?;");

			this.pstmt.setString(1, this.password);
			this.pstmt.setString(2, this.username);
			this.pstmt.setString(3, this.name);

			System.out.println(this.pstmt.toString());

			return this.pstmt.executeUpdate() == 1;
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deposit(String amount) throws MySQLIntegrityConstraintViolationException {
		try {
			this.getDBBalance();

			this.pstmt = this.connection.prepareStatement("UPDATE sbsa_users SET balance=? WHERE username=?;");
			this.pstmt.setDouble(1, this.balance + Double.parseDouble(amount));
			this.pstmt.setString(2, this.username);
			System.out.println(this.pstmt.toString());

			return this.pstmt.executeUpdate() == 1;
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public double getDBBalance() throws MySQLIntegrityConstraintViolationException {
		try {
			this.pstmt = this.connection.prepareStatement("SELECT balance FROM sbsa_users WHERE username=?;");
			this.pstmt.setString(1, this.username);
			System.out.println(this.pstmt.toString());

			this.rs = this.pstmt.executeQuery();
			this.rs.next();
			this.balance = rs.getDouble(1);
			return this.balance;
		} catch (MySQLIntegrityConstraintViolationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.balance;
	}
}
