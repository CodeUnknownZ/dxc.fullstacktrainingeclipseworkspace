package com.dxc.model;

import java.sql.Connection;

public class CustomerModel extends UserModel {
	
	protected final int isAdmin = 0;

	public CustomerModel() {
		super();
	}

	public CustomerModel(Connection connection) {
		super(connection);
	}
}
