package com.dxc.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.*;

public class ViewCustomerListHandler extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		try {
			AdminModel adminModel = new AdminModel();
			session.setAttribute("customerModelList", adminModel.getCustomerList());
			
			response.sendRedirect("/SimpleBankAssignment/Admin/CustomerList.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
