package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.*;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Servlet implementation class RegistrationHandler
 */
public class RegistrationHandler extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		try {
			UserModel userModel = new UserModel();

			userModel.setName(request.getParameter("name"));
			userModel.setUsername(request.getParameter("username"));
			userModel.setPassword(request.getParameter("password"));
			userModel.setConfirmPassword(request.getParameter("confirmPassword"));

			if (!userModel.checkConfirmPassword()) {
				session.setAttribute("errorMessage",
						"Passwords does not match please double check your intended password.");
				response.sendRedirect("/SimpleBankAssignment/RegistrationPage.jsp");
			} else {
				boolean success = false;
				if (request.getParameter("isAdmin") != null) {
					success = userModel.registerUser(request.getParameter("isAdmin").equals("on"));
				} else {
					success = userModel.registerUser(false);
				}

				if (success) {
					session.setAttribute("errorMessage", "");
					response.sendRedirect("/SimpleBankAssignment/LoginPage.jsp");
				} else {
					session.setAttribute("errorMessage",
							"An Error has occured while registering account, please try again.");
					response.sendRedirect("/SimpleBankAssignment/Error.jsp");
				}
			}
		} catch (MySQLIntegrityConstraintViolationException e) {
			session.setAttribute("errorMessage",
					"Sorry, " + request.getParameter("username") + " is already in use, please try a different name.");
			response.sendRedirect("/SimpleBankAssignment/RegistrationPage.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
