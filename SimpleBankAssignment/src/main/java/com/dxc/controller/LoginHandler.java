package com.dxc.controller;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.*;

public class LoginHandler extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		UserModel userModel = new UserModel();

		Boolean verified = userModel.verifyLogin(request.getParameter("username"), request.getParameter("password"));
		if (verified) {

			AdminModel adminModel = new AdminModel(userModel.getConnection());
			adminModel.setUsername(request.getParameter("username"));

			session.setAttribute("name", request.getParameter("name"));
			session.setAttribute("username", request.getParameter("username"));
			session.setAttribute("errorMessage", "");

			if (adminModel.checkIsAdmin()) {
				response.sendRedirect("/SimpleBankAssignment/Admin/index.jsp");
			} else {
				response.sendRedirect("/SimpleBankAssignment/Customer/index.jsp");
			}
		} else {
			session.setAttribute("errorMessage", "An Error has occured while logging in, please try again.");
			response.sendRedirect("/impleBankAssignment/Error.jsp");
		}
	}
}
