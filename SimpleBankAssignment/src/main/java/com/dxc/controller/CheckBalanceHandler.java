package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.*;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Servlet implementation class CheckBalanceHandler
 */
public class CheckBalanceHandler extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		try {
			UserModel userModel = new UserModel();
			userModel.setUsername(session.getAttribute("username").toString());
			session.setAttribute("balance", userModel.getDBBalance());
			response.sendRedirect("/SimpleBankAssignment/Customer/Balance.jsp");

		} catch (MySQLIntegrityConstraintViolationException e) {
			session.setAttribute("errorMessage", "Deposit unsuccessfull, please check your bills.");
			response.sendRedirect("/SimpleBankAssignment/Customer/Deposit.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
