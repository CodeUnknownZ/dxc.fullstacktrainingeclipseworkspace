package com.dxc.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dxc.model.*;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

/**
 * Servlet implementation class ForgetPasswordHandler
 */
public class ForgetPasswordHandler extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		try {
			UserModel userModel = new UserModel();

			userModel.setName(request.getParameter("name"));
			userModel.setUsername(request.getParameter("username"));
			userModel.setPassword(request.getParameter("password"));
			userModel.setConfirmPassword(request.getParameter("confirmPassword"));

			if (!userModel.checkConfirmPassword()) {
				session.setAttribute("errorMessage",
						"Passwords does not match please double check your intended password.");
				response.sendRedirect("/SimpleBankAssignment/ForgetPassword.jsp");
			} else {
				if (userModel.updatePassword()) {
					session.setAttribute("errorMessage", "");
					response.sendRedirect("/SimpleBankAssignment/LoginPage.jsp");
				} else {
					session.setAttribute("errorMessage",
							"Password change unsuccessfull, please check your name and username again.");
					response.sendRedirect("/SimpleBankAssignment/ForgetPassword.jsp");
				}
			}
		} catch (MySQLIntegrityConstraintViolationException e) {
			session.setAttribute("errorMessage",
					"Password change unsuccessfull, please check your name and username again.");
			response.sendRedirect("/SimpleBankAssignment/ForgetPassword.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
