<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,com.dxc.model.*"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Customer List</title>
<link rel="stylesheet" href="../main.css">
</head>
<body>
	<div>
		Below is the current list of customers.
		<%
	ArrayList<CustomerModel> custMdlList = (ArrayList<CustomerModel>) session.getAttribute("customerModelList");
	for (int i = 0; i < custMdlList.size(); i++) {
		UserModel userMdl = custMdlList.get(i);
		out.println("Name: " + userMdl.getName());
		out.println("Username: " + userMdl.getUsername());
	}
	%>
	</div>
</body>
</html>