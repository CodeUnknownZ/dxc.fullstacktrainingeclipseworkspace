<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Customer Home</title>
<link rel="stylesheet" href="../main.css">
</head>

<body>
	<div>
		<p>Welcome admin ${username}, Simple Bank System page.</p>
		<a class="button" href="/SimpleBankAssignment/ViewCustomerListHandler"> View Customer List </a>
		<a class="button" href="/SimpleBankAssignment/ViewPendingLoansHandler"> View Pending Loans </a>
		<a class="button" href="/SimpleBankAssignment/Admin/UpdateLoans.jsp"> Update Loan Status </a>
		<a class="button" href="/SimpleBankAssignment/Admin/AnalyticViewHandler"> Analysis View </a>
	</div>
</body>

</html>