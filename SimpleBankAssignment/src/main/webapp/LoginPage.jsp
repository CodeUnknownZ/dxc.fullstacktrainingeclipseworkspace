<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Login</title>
<link rel="stylesheet" href="./main.css">
</head>

<body>

	<div>
		<form action="/SimpleBankAssignment/LoginHandler">
			<label> Username </label> <input type="text" name="username">
			<label> Password</label> <input type="text" name="password">
			<button class="button formButton" type="submit">Login</button>
			<a class="button formButton" href="/SimpleBankAssignment/ForgetPassword.jsp">
				Forget Password </a>

		</form>

	</div>

</body>

</html>