<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<!DOCTYPE html>
	<html>

	<head>
		<meta charset="UTF-8">
		<title>Welcome</title>
		<link rel="stylesheet" href="./main.css">
	</head>

	<body>
		<div>
			<div class="errorPageDiv">${errorMessage}</div>
			<form action="/SimpleBankAssignment/RegistrationHandler">
				<label> Name </label>
				<input type="text" name="name">
				<label> Username </label>
				<input type="text" name="username">
				<label> Password</label>
				<input type="text" name="password" onCopy="return false" onCut="return false">
				<label> Confirm Password</label>
				<input type="text" name="confirmPassword" onpaste="return false;">

				<label hidden> Admin? </label>
				<input hidden type="checkbox" name="isAdmin">

				<button class="button formButton" type="submit"> Register Account </button>
			</form>
		</div>
	</body>

	</html>