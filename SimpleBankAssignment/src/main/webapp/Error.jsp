<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Error</title>
<link rel="stylesheet" href="./main.css">
</head>

<body>
	<div class="errorPageDiv">Oops ${errorMessage}</div>

	<p> You will be redirected in 3s... </p>
	<script type="text/javascript">
		setTimeout(function() {
			let message = "${errorMessage}";
			if (message.includes("logging in")
					|| message.includes("registering account")) {
				window.location.href = window.location.origin
						+ "/SimpleBankAssignment";
			} else {
				window.location.href = window.location.origin
						+ "/SimpleBankAssignment/LoginHandler";
			}
		}, 2500);
	</script>
</body>

</html>