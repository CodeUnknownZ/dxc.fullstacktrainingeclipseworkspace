<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Simple Bank System</title>
<link rel="stylesheet" href="./main.css">
</head>

<body>
	<div>
		Welcome to Simple Bank System page. <a class="button"
			href="/SimpleBankAssignment/LoginPage.jsp"> Login </a> <a class="button"
			href="/SimpleBankAssignment/RegistrationPage.jsp"> Registration </a>
	</div>
</body>

</html>