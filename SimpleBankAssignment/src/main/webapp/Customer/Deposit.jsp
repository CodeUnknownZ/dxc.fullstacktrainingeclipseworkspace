<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Deposit</title>
<link rel="stylesheet" href="../main.css">
</head>

<body>
	<div>
		<div class="errorPageDiv">${errorMessage}</div>
		<form action="/SimpleBankAssignment/DepositHandler">
			<label> Enter Amount to deposit (2-5000)</label> <input type="number"
				name="amount" min=2 max=5000 placeholder=0
				onblur="checkValue(this)">

			<button class="button formButton" type="submit">Deposit
				Money</button>
		</form>
	</div>

	<script type="text/javascript">
		function checkValue(input) {
			if (input.value > 5000) {
				input.value = 5000
			}
			if (input.value < 2) {
				input.value = ''
			}
		}
	</script>
</body>

</html>