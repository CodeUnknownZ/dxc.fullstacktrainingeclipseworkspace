<head>
<meta charset="UTF-8">
<title>Account Balance</title>
<link rel="stylesheet" href="../main.css">
</head>

<body>
	<div class="errorPageDiv">
		Dear ${username}, your balance is SGD${balance} <a class="button"
			href="/SimpleBankAssignment/Customer/index.jsp"> You will be
			redirected to home page after 10s...or click here to redirect now </a>
	</div>


	<script type="text/javascript">
		setTimeout(function() {
			window.location.href = window.location.origin
					+ "/SimpleBankAssignment/Customer/index.jsp";
		}, 10000);
	</script>
</body>

</html>