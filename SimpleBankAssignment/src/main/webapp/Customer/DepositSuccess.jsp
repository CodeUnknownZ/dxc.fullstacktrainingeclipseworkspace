<head>
<meta charset="UTF-8">
<title>Success</title>
<link rel="stylesheet" href="../main.css">
</head>

<body>
	<div class="errorPageDiv">
		Deposit successful, your new balance is ${balance} <a class="button"
			href="/SimpleBankAssignment/Customer/index.jsp"> You will be
			redirected to home page...or click here to redirect now </a>
	</div>


	<script type="text/javascript">
		setTimeout(function() {
			window.location.href = window.location.origin
					+ "/SimpleBankAssignment/Customer/index.jsp";
		}, 5000);
	</script>
</body>

</html>