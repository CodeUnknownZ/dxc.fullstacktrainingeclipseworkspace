<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Customer Home</title>
<link rel="stylesheet" href="../main.css">
</head>

<body>
	<div>
		<p>Welcome ${username}, Simple Bank System page.</p>
		<p>How can we be of service to you today.</p>
		<a class="button" href="/SimpleBankAssignment/CheckBalanceHandler"> Check Balance </a>
		<a class="button" href="/SimpleBankAssignment/Customer/Deposit.jsp"> Deposit </a>
	</div>
</body>

</html>