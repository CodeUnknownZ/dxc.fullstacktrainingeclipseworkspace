<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Forget Password</title>
<link rel="stylesheet" href="./main.css">
</head>
<body>
	<div>
		<div class="errorPageDiv">${errorMessage}</div>
		<form action="/SimpleBankAssignment/ForgetPasswordHandler">
				<label> Name </label>
				<input type="text" name="name">
				<label> Username </label>
				<input type="text" name="username">
				<label> New Password</label>
				<input type="text" name="password" onCopy="return false" onCut="return false">
				<label> Confirm Password</label>
				<input type="text" name="confirmPassword" onpaste="return false;">
				<button class="button formButton" type="submit"> Change Password </button>
		</form>
	</div>
</body>
</html>