package com.dxc.user.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dxc.user.enitiy.User;
import com.dxc.user.repository.UserRepository;
import com.dxc.user.value_objects.Department;
import com.dxc.user.value_objects.ResponseTemplateVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RestTemplate restTemplate;

	public User saveUser(User user) {
		log.info("Inside saveUser method of UserService");
		return userRepository.save(user);
	}

	public ResponseTemplateVO getUserWithDepartment(long userID) {
		ResponseTemplateVO vo = new ResponseTemplateVO();
		User user = userRepository.findByUserID(userID);
		Department department = restTemplate.getForObject("http://DEPARTMENT-SERVICE/departments/" + user.getDepartmentID(),
				Department.class);
		vo.setDepartment(department);
		vo.setUser(user);
		return vo;
	}
}
