package com.dxc.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.user.enitiy.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	User findByUserID(long userID);

}
