package com.dxc.user.value_objects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
	private long departmentID;
	private String departmentName;
	private String departmentAddress;
	private String departmentCode;
}
